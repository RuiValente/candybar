package api.errors

object ErrorMessages {
  val UNDEFINED_ERROR_MSG: String = "Ooops! Something went wrong!"
  val SUCCESSFULLY_INSERTED_CANDIES: String = "Candies inserted."
  val FAULTY_REQUEST = "Error in coin insertion."
  def needMoreMoney(value: Double): String = s"Need to insert $$$value more."
  def giveChange(value: Double): String = s"Pick up your candy. The change is $$$value."
  val NOT_VALID_REQUEST: String = "Request not valid."
  val NOT_VALID_USER: String = "User not valid."
  val NOT_VALID_COIN: String = "Coin not valid."
}
