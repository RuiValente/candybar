package api.dtos
import play.api.libs.json.{ Json, OFormat }

case class CreateBarsDTO(
  barName: String,
  price: Double)

object CreateBarsDTO {
  implicit val createBarsDTO: OFormat[CreateBarsDTO] = Json.format[CreateBarsDTO]
}