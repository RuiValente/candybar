package api.dtos
import api.defined.DefinedValues._
import api.errors.ErrorMessages._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json.{ JsonValidationError, Reads, _ }

case class InsertCoinDTO(
  customer: String,
  coin: Double)

object InsertCoinDTO {
  implicit val insertCoinDTO: Reads[InsertCoinDTO] = (
    (__ \ "customer").read[String] and
    (__ \ "coin").read[Double](filter[Double](JsonValidationError(NOT_VALID_COIN))(coinList.contains(_))))(InsertCoinDTO.apply _)
}
