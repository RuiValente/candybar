package api.defined

import api.dtos.CreateBarsDTO

object DefinedValues {
  //Bars inserted
  val candySeq: Seq[CreateBarsDTO] = Seq(
    CreateBarsDTO("Mars", 2.80),
    CreateBarsDTO("Snickers", 2.80),
    CreateBarsDTO("KitKat", 2.80),
    CreateBarsDTO("Bounty", 2.80))

  // Coins accepted
  val coinList: List[Double] = List(0.25, 0.5, 1.0)

  //Maximum costumers
  val maxCustomers: Int = 5

}
