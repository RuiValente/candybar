package api.controllers

import akka.actor.ActorSystem
import api.defined.DefinedValues._
import api.dtos.InsertCoinDTO
import api.errors.ErrorMessages._
import database.repository.InsertRepository
import javax.inject._
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.{ ExecutionContext, Future }

/** Class that is injected with end-points */

@Singleton class EntryController @Inject() (
  cc: ControllerComponents,
  actorSystem: ActorSystem,
  insertCoinActions: InsertRepository)(implicit exec: ExecutionContext) extends AbstractController(cc) {

  /**
   * Insertion of candies in the vending machine
   * @return OK if all the candies are inserted, otherwise Bad Request
   */
  def InsertCandies: Action[AnyContent] = Action.async {
    val resInsert: Future[Option[Int]] = insertCoinActions.insertCandies
    resInsert.map { optionInsertion =>
      optionInsertion.getOrElse(0) match {
        case x if x == candySeq.size => Ok(SUCCESSFULLY_INSERTED_CANDIES)
        case _ => Conflict(UNDEFINED_ERROR_MSG)
      }
    }
  }

  /**
   * Insert coin action
   * @return When a valid request is inserted, it is added in the database, otherwise an error message is sent
   */
  def InsertCoin(candy: String): Action[JsValue] = Action(parse.json).async { request: Request[JsValue] =>
    //Cleaning old rows before:
    insertCoinActions.deleteOldRows.flatMap { _ =>
      val insertCoin = request.body.validate[InsertCoinDTO]
      insertCoin.fold(
        _ => Future.successful { BadRequest(FAULTY_REQUEST) },
        entry => {
          insertCoinActions.userVerification(entry.customer).flatMap {
            case numberEntries if numberEntries <= maxCustomers => insertCoinActions.insertCoin(candy, entry).flatMap { sumDollars =>
              if (sumDollars >= 0) {
                insertCoinActions.candyBuy(candy, sumDollars, entry.customer).map {
                  case moneyChange if moneyChange > 0 => Ok(giveChange(moneyChange))
                  case moneyMissing => Ok(needMoreMoney(-moneyMissing))
                }
              } else Future.successful { BadRequest(NOT_VALID_REQUEST) }
            }
            case _ => Future.successful { BadRequest(NOT_VALID_USER) }
          }
        })
    }
  }
}
