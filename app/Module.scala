import com.google.inject.AbstractModule
import database.CreateTables
import database.properties.ProdDBProperties
import database.repository._

import scala.concurrent.ExecutionContext

/** Outline the database to be used implicitly */
class Module extends AbstractModule {

  override def configure(): Unit = {
    //3 minutes in milliseconds
    val timeLimit: Int = 180000

    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

    bind(classOf[CreateTables]).toInstance(new CreateTables(ProdDBProperties))
    bind(classOf[InsertRepository]).toInstance(new InsertRepositoryImpl(ProdDBProperties, timeLimit))

  }
}