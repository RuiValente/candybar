package database.repository

import api.dtos.InsertCoinDTO

import scala.concurrent.Future

trait InsertRepository {

  def insertCoin(candy: String, coin: InsertCoinDTO): Future[Double]
  def userVerification(user: String): Future[Int]
  def candyBuy(candy: String, sum: Double, customer: String): Future[Double]
  def deleteOldRows: Future[Int]
  def insertCandies: Future[Option[Int]]
}
