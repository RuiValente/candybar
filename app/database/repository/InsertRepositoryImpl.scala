package database.repository

import api.defined.DefinedValues._
import api.dtos.InsertCoinDTO
import database.mappings.CandyMappings._
import database.mappings.InsertionMappings._
import database.mappings.{ CandyRow, InsertRow }
import database.properties.DBProperties
import javax.inject.Inject
import slick.jdbc.H2Profile.api._

import scala.concurrent.{ ExecutionContext, Future }

/**  Class that receives a db path */
class InsertRepositoryImpl @Inject() (dbClass: DBProperties, timeLimit: Int)(implicit val executionContext: ExecutionContext) extends InsertRepository {

  val db = dbClass.db

  /**
   * Function that is used to insert the four Candies required
   * @return Future with number of rows deleted from table
   */
  def insertCandies: Future[Option[Int]] = {
    for {
      _ <- db.run(candyTable.delete)
      insertion <- db.run(candyTable ++= candySeq.map(candy => CandyRow(candy.barName, candy.price)))
    } yield insertion
  }

  /**
   * Function that verifies if the requested candy is in the vending machine. If so, it returns the amount
   * of money that was inserted by the user to that candy
   * @return -1.0 if the candy doesn't exist, otherwise the amount already inserted
   */
  def insertCoin(candy: String, coin: InsertCoinDTO): Future[Double] = {
    val resultCandy = db.run(candyTable.filter(_.candy === candy).result)
    resultCandy.flatMap(seqCandyRow => if (seqCandyRow.isEmpty) Future.successful { -1.0 } else insertCoinAux(candy, coin))
  }

  /**
   * Insert an user into database with is password encrypted
   * @return The number of insertions into database
   */
  private def insertCoinAux(candy: String, entry: InsertCoinDTO): Future[Double] = {

    val timeOfInsertion = System.currentTimeMillis()

    val sumCoins = candyTable
      .filter(_.candy === candy)
      .join(insertedCoinsTable
        .filter(_.user === entry.customer)
        .filter(_.candy === candy))
      .on(_.candy === _.candy)
      .map(_._2.coinValue)
      .result

    for {
      _ <- db.run(insertedCoinsTable += InsertRow(entry.customer, candy, entry.coin, timeOfInsertion))
      resultCoins <- db.run(sumCoins)
    } yield resultCoins.sum

  }

  //Verify if there are 5 users in the database already beyond coin.user
  def userVerification(user: String): Future[Int] = {
    val insertTableEntry = insertedCoinsTable.map(_.user).distinct.result
    db.run(insertTableEntry).map(seq => (seq :+ user).toSet.size)
  }

  /**
   * Function that verifies either if a candy is bought and respective change,
   * or how much money is left to purchase the candy
   * @return if the candy is bought the function return the change value,
   *         otherwise it returns the negative value of the money that is left
   */
  def candyBuy(candy: String, sum: Double, customer: String): Future[Double] = {

    val timeOfInsertion = System.currentTimeMillis()

    val deleteCandy = candyTable
      .filter(_.candy === candy)
      .delete

    val deleteCoinsRecord = insertedCoinsTable
      .filter(_.timeStamp >= timeOfInsertion - timeLimit)
      .filter(_.user === customer)
      .filter(_.candy === candy)
      .delete

    val price = db.run(candyTable
      .filter(_.candy === candy)
      .map(_.price).result)

    price.flatMap { seqPrice =>
      val seqPriceOption = seqPrice.headOption.getOrElse(0.0)
      val count = sum - seqPriceOption
      if (seqPriceOption < sum) {
        //Candy status is deleted from the stock table
        val res = for {
          //Delete the row of candy purchased
          _ <- db.run(deleteCandy)
          //Delete the rows used to buy the candy
          _ <- db.run(deleteCoinsRecord)
        } yield 0
        res.map(zero => math.round((zero + count) * 100).toDouble / 100)
      } else {
        //In case the user had bought the candy, it is returned a positive value with the change
        //Candy not sold, return negative number that refers to quantity missing
        Future.successful { math.round(count * 100).toDouble / 100 }
      }
    }
  }

  /**
   * Function that deletes the old rows from the table
   * @return Future with number of rows deleted from table
   */
  def deleteOldRows: Future[Int] = {
    val timeOfInsertion = System.currentTimeMillis()
    val deleteOldRows = insertedCoinsTable
      .filter(_.timeStamp < timeOfInsertion - timeLimit)
      .delete
    db.run(deleteOldRows)
  }
}
