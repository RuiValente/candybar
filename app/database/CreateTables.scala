package database

import database.mappings.CandyMappings.candyTable
import database.mappings.InsertionMappings.insertedCoinsTable
import database.properties.DBProperties
import slick.jdbc.H2Profile.api._
import javax.inject.Inject
import javax.inject.Singleton

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class CreateTables @Inject() (dbClass: DBProperties) {
  private def escapeTicks(strIter: Iterable[String]): String = strIter.mkString(" ").replace("\"", "")

  private def createIfNotExists(strIter: Iterable[String]): String = escapeTicks(strIter).replace("create table", "CREATE TABLE IF NOT EXISTS")

  val db = dbClass.db

  val candyTableSql = sqlu"#${createIfNotExists(candyTable.schema.create.statements)}"
  val insertedCoinsTableSql = sqlu"#${createIfNotExists(insertedCoinsTable.schema.create.statements)}"

  val setupAction = DBIO.seq(candyTableSql, insertedCoinsTableSql)

  Await.result(db.run(setupAction), Duration.Inf)
}
