package database.mappings

import slick.jdbc.H2Profile.api._

/**  Case class of Login Table Row */

case class InsertRow(
  user: String,
  candy: String,
  coinValue: Double,
  timeStamp: Long)

/**
 * Insert the request of the user in the table, iff these is valid
 * @param tag slick tag
 */
class InsertionTable(tag: Tag) extends Table[InsertRow](tag, _tableName = "inserts") {
  def user: Rep[String] = column[String]("user")
  def candy: Rep[String] = column[String]("candy")
  def coinValue: Rep[Double] = column[Double]("coinValue")
  def timeStamp: Rep[Long] = column[Long]("timeStamp")

  def * = (user, candy, coinValue, timeStamp) <> (InsertRow.tupled, InsertRow.unapply)
}

/** Queries of user table and login table */
object InsertionMappings {
  lazy val insertedCoinsTable = TableQuery[InsertionTable]
}
