package database.repostory

import api.defined.DefinedValues._
import api.dtos.InsertCoinDTO
import database.CreateTables
import database.mappings.CandyMappings._
import database.mappings.InsertionMappings._
import database.properties.TestDBProperties
import database.repository.InsertRepositoryImpl
import org.scalatest.{ Matchers, _ }
import slick.jdbc.H2Profile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext, Future }
class InsertRepositoryTest extends AsyncWordSpec with BeforeAndAfterAll with BeforeAndAfterEach with Matchers {

  implicit private val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  lazy implicit private val db: Database = TestDBProperties.db

  /** It is defined a timeLimit of 3 seconds instead of 3 minutes for the tests*/
  private val userActions = new InsertRepositoryImpl(TestDBProperties, timeLimit = 3000)

  private val tables = Seq(candyTable, insertedCoinsTable)

  override def beforeAll(): Unit = {
    new CreateTables(TestDBProperties)
  }

  override def afterAll(): Unit = {
    Await.result(db.run(DBIO.seq(tables.map(_.schema.drop): _*)), Duration.Inf)
  }

  override def beforeEach(): Unit = {
    Await.result(db.run(DBIO.seq(tables.map(_.delete): _*)), Duration.Inf)
  }

  /** Verify if candies are inserted in the database correctly */
  "InsertRepository #insertCandies" should {
    "check if the candies are inserted in candy table in database" in {

      val result = for {
        _ <- userActions.insertCandies
        resultCandiesTable <- db.run(candyTable.result)
      } yield resultCandiesTable

      result.map { resultCandiesTable =>
        resultCandiesTable.size shouldEqual 4
        resultCandiesTable.forall(candySeq.map(_.barName) contains _.candy) shouldBe true
      }
    }
  }

  /** Verify if the function return -1.0 since the filter will return no candies  */
  "InsertRepository #insertCoin" should {
    "check if return -1.0 when no candies were inserted" in {

      val result = for {
        resultSumCoins <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 0.5))
      } yield resultSumCoins

      result.map(_ shouldEqual -1.0)
    }
  }

  /** Verify if a coin of 0.5$ is inserted correctly */
  "InsertRepository #insertCoin" should {
    "check if the sum of coins is correct" in {

      val result = for {
        _ <- userActions.insertCandies
        resultSumCoins <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 0.5))
      } yield resultSumCoins

      result.map(_ shouldEqual 0.5)
    }
  }

  /** Verify if insertion of 2 coins is correctly proceeded */
  "InsertRepository #insertCoin" should {
    "check if the sum of 2 coins is correct" in {

      val result = for {
        _ <- userActions.insertCandies
        _ <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 1.0))
        resultSumCoins <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 0.5))
      } yield resultSumCoins

      result.map(_ shouldEqual 1.5)
    }
  }

  /** Verify if a coin is inserted correctly in the database */
  "InsertRepository #insertCoin" should {
    "check if the coin is inserted in insertedCoins table in database" in {

      val result = for {
        _ <- userActions.insertCandies
        _ <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 0.5))
        resultInsertedCoinTable <- db.run(insertedCoinsTable.result)
      } yield resultInsertedCoinTable

      result.map { seqUserRow =>
        seqUserRow.forall(_.user === "John") shouldBe true
        seqUserRow.forall(_.candy === "Mars") shouldBe true
        seqUserRow.forall(_.coinValue === 0.5) shouldBe true
      }
    }
  }

  /** Verify if user is inserted correctly in the database */
  "InsertRepository #userVerification" should {
    "check the number of 0 active users in database" in {

      val result = for {
        _ <- userActions.insertCandies
        resultUserVerification <- userActions.userVerification(user = "John")
      } yield resultUserVerification

      result.map(_ shouldEqual 1)
    }
  }

  /** Verify if 5 different users are correctly inserted in the database */
  "InsertRepository #userVerification" should {
    "check the number of 5 active users in database" in {
      val nameSeq = Seq("John", "Oliver", "Harry", "George", "Charlie")
      val result = for {
        _ <- userActions.insertCandies
        //Insertion of coins from 5 different customers
        _ <- Future.sequence { nameSeq.map(name => userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO(name, 0.5))) }
        //User verification of an user already registered
        resultUserVerificationSame <- userActions.userVerification(user = "John")
        //User verification of an user not registered
        resultUserVerificationDifferent <- userActions.userVerification(user = "Leo")
      } yield (resultUserVerificationSame, resultUserVerificationDifferent)

      //Tests to verify if the return of the userVerification function is correct
      result.map(_._1 shouldEqual nameSeq.size)
      result.map(_._2 shouldEqual nameSeq.size + 1)
    }
  }

  /**
   * Verify if the function candyBuy is able to return the money that
   * still needed to be inserted to purchase the candy  desired
   */
  "InsertRepository #candyBuy" should {
    "check if the quantity that is left to buy a snack is correct" in {

      val result = for {
        //Insertion of candies
        _ <- userActions.insertCandies
        //Insertion of a coin in order to be registered in the database
        _ <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 1.0))
        //Function that processes the overall quantity of many already inserted by the user
        resultLeft <- userActions.candyBuy(candy = "Mars", sum = 1.0, customer = "John")
        resultCandiesTable <- db.run(candyTable.result)
        resultInsertionTable <- db.run(insertedCoinsTable.result)
      } yield (resultLeft, resultCandiesTable, resultInsertionTable)

      result.map {
        case (resultLeft, resultCandiesTable, resultInsertionTable) =>
          //Verify if the money left is correct
          resultLeft shouldEqual -1.8
          //Verify if the table has the original 4 rows
          resultCandiesTable.size shouldEqual 4
          //Verify if the record of the user remains
          resultInsertionTable.size shouldEqual 1
          //Verify if the bars remain in the table
          resultCandiesTable.forall(candySeq.map(_.barName) contains _.candy) shouldBe true
          //Verify if the record of the request from the user remains in the insertedCoinsTable
          resultInsertionTable.forall(_.candy === "Mars") shouldBe true
          resultInsertionTable.forall(_.user === "John") shouldBe true
          resultInsertionTable.forall(_.coinValue === 1) shouldBe true
      }
    }
  }

  /** Verify if the function candyBuy could delete the correct rows after a purchase */
  "InsertRepository #candyBuy" should {
    "check the change and deleted rows from the database" in {

      val result = for {
        //Insertion of candies
        _ <- userActions.insertCandies
        //Insertion of a coin in order to be registered in the database
        _ <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 1.0))
        //Function that processes the overall quantity of many already inserted by the user
        resultLeft <- userActions.candyBuy(candy = "Mars", sum = 4.0, customer = "John")
        resultCandiesTable <- db.run(candyTable.result)
        resultInsertionTable <- db.run(insertedCoinsTable.result)
      } yield (resultLeft, resultCandiesTable, resultInsertionTable)

      result.map {
        case (resultLeft, resultCandiesTable, resultInsertionTable) =>
          //Verify if the change is correct
          resultLeft shouldEqual 1.2
          //Verify if the table has 3 rows instead of the original 4, because of the deletion of row "Mars"
          resultCandiesTable.size shouldEqual 3
          //Verify if the other bars remain in the table
          resultCandiesTable.forall(candySeq.map(_.barName) contains _.candy) shouldBe true
          //Verify if the row referent to tha bar "Mars" is no longer in the table
          resultCandiesTable.forall(_.candy != "Mars") shouldBe true
          //Verify if the record of the user is also deleted
          resultInsertionTable shouldBe empty
      }
    }
  }

  /** Verify if the outdated rows are deleted properly */
  "InsertRepository #deleteOldRows" should {
    "check if function is able to delete the outdated rows from insertedCoins table in database" in {

      val result = for {
        //Insertion of candies
        _ <- userActions.insertCandies
        //Insertion of a coin in order to be registered in the database
        _ <- userActions.insertCoin(candy = "Mars", coin = InsertCoinDTO("John", 1.0))
        //Wait for 4 seconds
        _ <- Future { Thread.sleep(4000) }
        //Delete the rows that are outdated
        resultLeft <- userActions.deleteOldRows
        resultInsertionTable <- db.run(insertedCoinsTable.result)
      } yield (resultLeft, resultInsertionTable)

      result.map {
        case (resultLeft, resultInsertionTable) =>
          //Verify if the return of the function deleteOldRows is correct
          resultLeft shouldEqual 1
          //Verify if the row in insertedCoinsTable was deleted
          resultInsertionTable shouldBe empty
      }
    }
  }

}