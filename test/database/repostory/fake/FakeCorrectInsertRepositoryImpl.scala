package database.repostory.fake

import api.dtos.InsertCoinDTO
import database.repository.InsertRepository

import scala.concurrent.Future

/** Fake Repository test to assist the Repository Unit tests*/
class FakeCorrectInsertRepositoryImpl extends InsertRepository {

  def insertCandies: Future[Option[Int]] = {
    Future.successful(Option(4))
  }

  def insertCoin(candy: String, coin: InsertCoinDTO): Future[Double] = {
    Future.successful(1.0)
  }

  def userVerification(user: String): Future[Int] = {
    Future.successful(1)
  }

  def candyBuy(candy: String, sum: Double, customer: String): Future[Double] = {
    Future.successful(1.0)
  }

  def deleteOldRows: Future[Int] = {
    Future.successful(0)
  }
}
