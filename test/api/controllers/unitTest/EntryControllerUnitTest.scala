package api.controllers.unitTest

import akka.stream.Materializer
import api.controllers.EntryController
import api.controllers.unitTest.UnitControllerTestsAppBuilder._
import database.repository.InsertRepository
import database.repostory.fake.{ FakeCorrectInsertRepositoryImpl, FakeWrongInsertCandiesRepositoryImpl, FakeWrongUserVerificationRepositoryImpl }
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.mvc.Results
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.ExecutionContext

class EntryControllerUnitTest extends PlaySpec with GuiceOneAppPerSuite with BeforeAndAfterAll with BeforeAndAfterEach with Results {
  implicit private val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  lazy implicit private val mat: Materializer = injector.instanceOf[Materializer]

  private val chatActions: InsertRepository = new FakeCorrectInsertRepositoryImpl()
  private val chatWrongAction: InsertRepository = new FakeWrongInsertCandiesRepositoryImpl()
  private val chatWrongUserAction: InsertRepository = new FakeWrongUserVerificationRepositoryImpl()

  /** Verify if the end-point candyBars is running properly */
  "EntryControllerUnit #InsertCandies" should {
    "send an OK if insertion of candies is completed" in {
      val controller = new EntryController(cc, actorSystem, chatActions)
      val result = controller.InsertCandies.apply(FakeRequest(POST, "/candyBars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000"))
      status(result) mustBe OK
    }
  }

  /** Verify if the end-point returns a conflict if the candies are not inserted in database */
  "EntryControllerUnit #InsertCandies" should {
    "send an CONFLICT if insertion of candies is not completed" in {
      val controller = new EntryController(cc, actorSystem, chatWrongAction)
      val result = controller.InsertCandies.apply(FakeRequest(POST, "/candyBars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000"))
      status(result) mustBe CONFLICT
    }
  }

  /**Verify if the end-point /candy/:candyName is running properly */
  "EntryControllerUnit #InsertCoin" should {
    "send an OK if insertion of coin is completed" in {
      val controller = new EntryController(cc, actorSystem, chatActions)
      val result = controller.InsertCoin(candy = "Mars").apply(FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(
          fields = "customer" -> "John",
          "coin" -> 0.5))))
      status(result) mustBe OK
    }
  }

  /** Verify if the end-point returns a Bad request with wrong json body fields*/
  "EntryControllerUnit #InsertCoin" should {
    "send a BAD REQUEST if Json body as the wrong format" in {
      val controller = new EntryController(cc, actorSystem, chatActions)
      val result = controller.InsertCoin(candy = "Mars").apply(FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(
          fields = "wrongCustomer" -> "John",
          "wrongCoin" -> 0.5))))
      status(result) mustBe BAD_REQUEST
    }
  }

  /** Verify if a Bad request is returned when more than 5 users are tried to be inserted*/
  "EntryControllerUnit #InsertCoin" should {
    "send a BAD REQUEST if insertion of more than 5 users is attempted" in {
      val controller = new EntryController(cc, actorSystem, chatWrongUserAction)
      val result = controller.InsertCoin(candy = "Mars").apply(FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(
          fields = "customer" -> "John",
          "coin" -> 0.5))))
      status(result) mustBe BAD_REQUEST
    }
  }

  /** Verify if the end-point /candy/:candyName returns a bad Request when a wrong coin is inserted*/
  "EntryControllerUnit #InsertCoin" should {
    "send an BAD REQUEST if coin has a wrong value" in {
      val controller = new EntryController(cc, actorSystem, chatActions)
      val result = controller.InsertCoin(candy = "Mars").apply(FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(
          fields = "customer" -> "John",
          "coin" -> 0.6))))
      status(result) mustBe BAD_REQUEST
    }
  }

}
