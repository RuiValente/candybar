package api.controllers.unitTest

import akka.actor.ActorSystem
import akka.stream.Materializer
import database.repository.InsertRepository
import database.repostory.fake.FakeCorrectInsertRepositoryImpl
import play.api.Mode
import play.api.inject.{ Injector, bind }
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.mvc.ControllerComponents
import javax.inject.Singleton

@Singleton
object UnitControllerTestsAppBuilder {

  val appBuilder: GuiceApplicationBuilder = new GuiceApplicationBuilder().in(Mode.Test)
    .load(
      new play.api.inject.BuiltinModule,
      new play.api.i18n.I18nModule,
      new play.api.mvc.CookiesModule,
      bind(classOf[InsertRepository]).toInstance(new FakeCorrectInsertRepositoryImpl()))

  lazy val injector: Injector = appBuilder.injector()

  lazy val mat: Materializer = injector.instanceOf[Materializer]
  lazy val cc: ControllerComponents = injector.instanceOf[ControllerComponents]
  val actorSystem: ActorSystem = injector.instanceOf[ActorSystem]

}
