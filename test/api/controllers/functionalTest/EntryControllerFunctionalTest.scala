package api.controllers.functionalTest

import database.CreateTables
import database.mappings.CandyMappings.candyTable
import database.mappings.InsertionMappings.insertedCoinsTable
import database.properties.TestDBProperties
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers.{ route, status, _ }
import slick.jdbc.H2Profile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext }
class EntryControllerFunctionalTest extends PlaySpec with GuiceOneAppPerSuite with BeforeAndAfterAll with BeforeAndAfterEach {

  implicit private val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  lazy implicit private val db: Database = TestDBProperties.db

  private val tables = Seq(candyTable, insertedCoinsTable)

  override def beforeAll(): Unit = {
    new CreateTables(TestDBProperties)
  }

  override def afterAll(): Unit = {
    Await.result(db.run(DBIO.seq(tables.map(_.schema.drop): _*)), Duration.Inf)
  }

  override def beforeEach(): Unit = {
    Await.result(db.run(DBIO.seq(tables.map(_.delete): _*)), Duration.Inf)
  }

  /** Insertion of a coin with no candies in the vending machine */
  "EntryControllerFunctional #InsertCoinWithNoCandies" should {
    "refuse a purchase attempt of a candy bar with no candies in the machine" in {
      /**Insertion of 1$ by John*/
      val InsertCoin1 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin1 = route(app, InsertCoin1)
      status(resultInsertCoin1.get) mustBe BAD_REQUEST
      contentAsString(resultInsertCoin1.get) mustBe "Request not valid."
    }
  }

  /** Request of a candy bar that does not exists in the vending machine */
  "EntryControllerFunctional #InsertCoinWithNoCandies" should {
    "refuse a purchase attempt of a candy bar that does not exist in the vending machine" in {

      /**Insertion of candies */
      val InsertCandiesRequest1 = FakeRequest(POST, "/candyBars")
        .withHeaders(HOST -> "localhost:9000")

      val resultInsertCandies1 = route(app, InsertCandiesRequest1)
      status(resultInsertCandies1.get) mustBe OK
      contentAsString(resultInsertCandies1.get) mustBe "Candies inserted."

      /**Insertion of 1$ by John*/
      val InsertCoin1 = FakeRequest(POST, "/candy/Milka")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin1 = route(app, InsertCoin1)
      status(resultInsertCoin1.get) mustBe BAD_REQUEST
      contentAsString(resultInsertCoin1.get) mustBe "Request not valid."
    }
  }

  /**
   * Insertion of different valued and not allowed coins from a unique user
   * in order to purchase a unique candy bar
   */
  "EntryControllerFunctional #InsertDifferentCoinsUniqueUsers" should {
    "purchase a candy bar with different coin insertions by a unique user" in {
      /**Insertion of candies */
      val InsertCandiesRequest1 = FakeRequest(POST, "/candyBars")
        .withHeaders(HOST -> "localhost:9000")

      val resultInsertCandies1 = route(app, InsertCandiesRequest1)
      status(resultInsertCandies1.get) mustBe OK
      contentAsString(resultInsertCandies1.get) mustBe "Candies inserted."

      /**New insertion of candies to verify if the database has no changes*/
      val InsertCandiesRequest2 = FakeRequest(POST, "/candyBars")
        .withHeaders(HOST -> "localhost:9000")

      val resultInsertCandies2 = route(app, InsertCandiesRequest2)
      status(resultInsertCandies2.get) mustBe OK
      contentAsString(resultInsertCandies2.get) mustBe "Candies inserted."

      /**Insertion of a wrong coin of 2$ by John*/
      val InsertWrongCoin = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 2.0)))

      val resultInsertWrongCoin = route(app, InsertWrongCoin)
      status(resultInsertWrongCoin.get) mustBe BAD_REQUEST
      contentAsString(resultInsertWrongCoin.get) mustBe "Error in coin insertion."

      /**Insertion of 1$ by John*/
      val InsertCoin1 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin1 = route(app, InsertCoin1)
      status(resultInsertCoin1.get) mustBe OK
      contentAsString(resultInsertCoin1.get) mustBe "Need to insert $1.8 more."

      /**Insertion of 1$ more by John*/
      val InsertCoin2 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin2 = route(app, InsertCoin2)
      status(resultInsertCoin2.get) mustBe OK
      contentAsString(resultInsertCoin2.get) mustBe "Need to insert $0.8 more."

      /**Insertion of 1$ more by John*/
      val InsertCoin3 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 0.5)))

      val resultInsertCoin3 = route(app, InsertCoin3)
      status(resultInsertCoin3.get) mustBe OK
      contentAsString(resultInsertCoin3.get) mustBe "Need to insert $0.3 more."

      /**Insertion of 0.25$ more by John*/
      val InsertCoin4 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 0.25)))

      val resultInsertCoin4 = route(app, InsertCoin4)
      status(resultInsertCoin4.get) mustBe OK
      contentAsString(resultInsertCoin4.get) mustBe "Need to insert $0.05 more."

      /**Insertion of 0.25$ more by John*/
      val InsertCoin5 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 0.25)))

      val resultInsertCoin5 = route(app, InsertCoin5)
      status(resultInsertCoin5.get) mustBe OK
      contentAsString(resultInsertCoin5.get) mustBe "Pick up your candy. The change is $0.2."

    }
  }

  /**
   * Insertion of different valued coins from a unique user in order to purchase a unique candy
   * bar proceeded by a new insertion of candies to buy the same candy bar
   */
  "EntryControllerFunctional #RenewVendingMachine" should {
    "purchase a candy bar and insert the candies again" in {
      /**Insertion of candies */
      val InsertCandiesRequest1 = FakeRequest(POST, "/candyBars")
        .withHeaders(HOST -> "localhost:9000")

      val resultInsertCandies1 = route(app, InsertCandiesRequest1)
      status(resultInsertCandies1.get) mustBe OK
      contentAsString(resultInsertCandies1.get) mustBe "Candies inserted."

      /**Insertion of 1$ by John*/
      val InsertCoin1 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin1 = route(app, InsertCoin1)
      status(resultInsertCoin1.get) mustBe OK
      contentAsString(resultInsertCoin1.get) mustBe "Need to insert $1.8 more."

      /**Insertion of 1$ by John*/
      val InsertCoin2 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin2 = route(app, InsertCoin2)
      status(resultInsertCoin2.get) mustBe OK
      contentAsString(resultInsertCoin2.get) mustBe "Need to insert $0.8 more."

      /**Insertion of 1$ by John*/
      val InsertCoin3 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin3 = route(app, InsertCoin3)
      status(resultInsertCoin3.get) mustBe OK
      contentAsString(resultInsertCoin3.get) mustBe "Pick up your candy. The change is $0.2."

      /**Insertion of 1$ by John*/
      val InsertCoin4 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin4 = route(app, InsertCoin4)
      status(resultInsertCoin4.get) mustBe BAD_REQUEST
      contentAsString(resultInsertCoin4.get) mustBe "Request not valid."

      /**New insertion of candies */
      val InsertCandiesRequest2 = FakeRequest(POST, "/candyBars")
        .withHeaders(HOST -> "localhost:9000")

      val resultInsertCandies2 = route(app, InsertCandiesRequest2)
      status(resultInsertCandies2.get) mustBe OK
      contentAsString(resultInsertCandies2.get) mustBe "Candies inserted."

      /**Insertion of 1$ by John*/
      val InsertCoin5 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin5 = route(app, InsertCoin5)
      status(resultInsertCoin5.get) mustBe OK
      contentAsString(resultInsertCoin5.get) mustBe "Need to insert $1.8 more."

      /**Insertion of 1$ by John*/
      val InsertCoin6 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin6 = route(app, InsertCoin6)
      status(resultInsertCoin6.get) mustBe OK
      contentAsString(resultInsertCoin6.get) mustBe "Need to insert $0.8 more."

      /**Insertion of 1$ by John*/
      val InsertCoin7 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin7 = route(app, InsertCoin7)
      status(resultInsertCoin7.get) mustBe OK
      contentAsString(resultInsertCoin7.get) mustBe "Pick up your candy. The change is $0.2."
    }
  }

  /** Insertion of coins by two different users in order to purchase a unique candy bar */
  "EntryControllerFunctional #InsertCoinsTwoUsers" should {
    "purchase a candy bar correctly when two users try to purchase it" in {
      /**Insertion of candies */
      val InsertCandiesRequest = FakeRequest(POST, "/candyBars")
        .withHeaders(HOST -> "localhost:9000")

      val resultInsertCandies = route(app, InsertCandiesRequest)
      status(resultInsertCandies.get) mustBe OK
      contentAsString(resultInsertCandies.get) mustBe "Candies inserted."

      /**Insertion of 1$ by John*/
      val InsertCoin1 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin1 = route(app, InsertCoin1)
      status(resultInsertCoin1.get) mustBe OK
      contentAsString(resultInsertCoin1.get) mustBe "Need to insert $1.8 more."

      /**Insertion of 1$ by Harry*/
      val InsertCoin2 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "Harry", "coin" -> 1.0)))

      val resultInsertCoin2 = route(app, InsertCoin2)
      status(resultInsertCoin2.get) mustBe OK
      contentAsString(resultInsertCoin2.get) mustBe "Need to insert $1.8 more."

      /**Insertion of 1$ more by John*/
      val InsertCoin3 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin3 = route(app, InsertCoin3)
      status(resultInsertCoin3.get) mustBe OK
      contentAsString(resultInsertCoin3.get) mustBe "Need to insert $0.8 more."

      /**Insertion of 1$ more by Harry*/
      val InsertCoin4 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "Harry", "coin" -> 1.0)))

      val resultInsertCoin4 = route(app, InsertCoin4)
      status(resultInsertCoin4.get) mustBe OK
      contentAsString(resultInsertCoin4.get) mustBe "Need to insert $0.8 more."

      /**Insertion of 1$ more by John*/
      val InsertCoin5 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

      val resultInsertCoin5 = route(app, InsertCoin5)
      status(resultInsertCoin5.get) mustBe OK
      contentAsString(resultInsertCoin5.get) mustBe "Pick up your candy. The change is $0.2."

      /**Insertion of 1$ more by Harry*/
      val InsertCoin6 = FakeRequest(POST, "/candy/Mars")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "customer" -> "Harry", "coin" -> 1.0)))

      val resultInsertCoin6 = route(app, InsertCoin6)
      status(resultInsertCoin6.get) mustBe BAD_REQUEST
      contentAsString(resultInsertCoin6.get) mustBe "Request not valid."
    }

    /**
     * Insertion of coins by five different users and respective waiting time
     * to clean the record of users in order to add the sixth
     */
    "EntryControllerFunctional #InsertCoinsSixUsers" should {
      "insert the coins of six different users" in {
        /** Insertion of candies */
        val InsertCandiesRequest = FakeRequest(POST, "/candyBars")
          .withHeaders(HOST -> "localhost:9000")

        val resultInsertCandies = route(app, InsertCandiesRequest)
        status(resultInsertCandies.get) mustBe OK
        contentAsString(resultInsertCandies.get) mustBe "Candies inserted."

        /** Insertion of 1$ by John */
        val InsertUser1 = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

        val resultInsertUser1 = route(app, InsertUser1)
        status(resultInsertUser1.get) mustBe OK
        contentAsString(resultInsertUser1.get) mustBe "Need to insert $1.8 more."

        /** Insertion of 1$ by Harry */
        val InsertUser2 = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "Harry", "coin" -> 1.0)))

        val resultInsertUser2 = route(app, InsertUser2)
        status(resultInsertUser2.get) mustBe OK
        contentAsString(resultInsertUser2.get) mustBe "Need to insert $1.8 more."

        /** Insertion of 1$ by Oliver */
        val InsertUser3 = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "Oliver", "coin" -> 1.0)))

        val resultInsertUser3 = route(app, InsertUser3)
        status(resultInsertUser3.get) mustBe OK
        contentAsString(resultInsertUser3.get) mustBe "Need to insert $1.8 more."

        /** Insertion of 1$ by George */
        val InsertUser4 = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "George", "coin" -> 1.0)))

        val resultInsertUser4 = route(app, InsertUser4)
        status(resultInsertUser4.get) mustBe OK
        contentAsString(resultInsertUser4.get) mustBe "Need to insert $1.8 more."

        /** Insertion of 1$ by Charlie */
        val InsertUser5 = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "Charlie", "coin" -> 1.0)))

        val resultInsertUser5 = route(app, InsertUser5)
        status(resultInsertUser5.get) mustBe OK
        contentAsString(resultInsertUser5.get) mustBe "Need to insert $1.8 more."

        /** Insertion of 1$ by Leo */
        val InsertUser6 = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "Leo", "coin" -> 1.0)))

        val resultInsertUser6 = route(app, InsertUser6)
        status(resultInsertUser6.get) mustBe BAD_REQUEST
        contentAsString(resultInsertUser6.get) mustBe "User not valid."

        /**Await time of 4 seconds since for the tests the 3 minutes update was compressed in 3 seconds*/
        Thread.sleep(4000)

        /** New attempt of 1$ insertion by Leo */
        val InsertUser6Again = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "Leo", "coin" -> 1.0)))

        val resultInsertUser6Again = route(app, InsertUser6Again)
        status(resultInsertUser6Again.get) mustBe OK
        contentAsString(resultInsertUser6Again.get) mustBe "Need to insert $1.8 more."

        /** New insertion of John to verify if the 1$ is not accounted */
        val InsertUser1Again = FakeRequest(POST, "/candy/Mars")
          .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
          .withBody(Json.toJson(Json.obj(fields = "customer" -> "John", "coin" -> 1.0)))

        val resultInsertUser1Again = route(app, InsertUser1Again)
        status(resultInsertUser1Again.get) mustBe OK
        contentAsString(resultInsertUser1Again.get) mustBe "Need to insert $1.8 more."

      }
    }

  }
}
