
import com.google.inject.AbstractModule
import database.CreateTables
import database.properties.TestDBProperties
import database.repository._

import scala.concurrent.ExecutionContext

class Module extends AbstractModule {
  override def configure(): Unit = {

    //3 seconds in milliseconds
    val timeLimit: Int = 3000
    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

    bind(classOf[CreateTables]).toInstance(new CreateTables(TestDBProperties))
    bind(classOf[InsertRepository]).toInstance(new InsertRepositoryImpl(TestDBProperties, timeLimit))

  }
}