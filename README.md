# Candybar

Create a Play application that simulates a candy bar vending machine.

## Getting Started

Each candy bar costs $2.80 and customers have a combination of $0.25, $0.50 & $1 coins.

The application must expose an API that allows users to purchase a candy bar by submitting a single coin at a time until the candy bar is paid for.
The vending machine has 4 candy bars in stock.
Once a customer starts a transaction, every insertion is aborted after 3 minutes. This means they have to complete it within 3 minutes else the vending machine will abort it.
The application should support at most 5 concurrent clients.
Fork and send me a Pull Request with your solution.

### How to use?

1. To start adding 4 candies to the machine:

```
POST /candyBars
```

2. To insert a coin and choose the candy (you have Bounty, KitKat, Snickers and Mars available):

```
POST /candy/:candyName
```

With Json Body:

```
{
	"customer": "John",
	"coin": 0.5
}
```

## Running the tests
For the tests, the transactions are aborted after 3 seconds, to avoid 3 minutes of waiting time.
Besides this, the remnant features stay the same.
To run the tests:

```
sbt test
```

To get the coverage Report of the tests:

```
sbt clean coverage test
sbt coverageReport
```

And access to the file:

```
target/scala-2.12/scoverage-report/index.html
```